#!/bin/bash
set -e

sudo pacman -S --noconfirm --needed moka-icon-theme
paru -S --noconfirm --needed mint-y-icons
paru -S --noconfirm --needed capitaine-cursors
